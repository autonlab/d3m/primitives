{
  "algorithm_types": [
    "LOGISTIC_REGRESSION"
  ],
  "name": "sklearn.linear_model.logistic.LogisticRegression",
  "primitive_family": "CLASSIFICATION",
  "python_path": "d3m.primitives.classification.logistic_regression.SKlearn",
  "source": {
    "name": "JPL",
    "contact": "mailto:shah@jpl.nasa.gov",
    "uris": [
      "https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues",
      "https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html"
    ]
  },
  "version": "2019.4.4",
  "id": "b9c81b40-8ed1-3b23-80cf-0d6fe6863962",
  "installation": [
    {
      "type": "PIP",
      "package_uri": "git+https://gitlab.com/datadrivendiscovery/sklearn-wrap.git@594f2a84ddd1e8b68fde672f21a746dcda7678e9#egg=sklearn_wrap"
    }
  ],
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
  "original_python_path": "sklearn_wrap.SKLogisticRegression.SKLogisticRegression",
  "primitive_code": {
    "class_type_arguments": {
      "Inputs": "d3m.container.pandas.DataFrame",
      "Outputs": "d3m.container.pandas.DataFrame",
      "Params": "sklearn_wrap.SKLogisticRegression.Params",
      "Hyperparams": "sklearn_wrap.SKLogisticRegression.Hyperparams"
    },
    "interfaces_version": "2019.5.8",
    "interfaces": [
      "supervised_learning.SupervisedLearnerPrimitiveBase",
      "base.PrimitiveBase",
      "base.ProbabilisticCompositionalityMixin"
    ],
    "hyperparams": {
      "penalty": {
        "type": "d3m.metadata.hyperparams.Enumeration",
        "default": "l2",
        "structural_type": "str",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Used to specify the norm used in the penalization. The 'newton-cg', 'sag' and 'lbfgs' solvers support only l2 penalties.",
        "values": [
          "l1",
          "l2"
        ]
      },
      "dual": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Dual or primal formulation. Dual formulation is only implemented for l2 penalty with liblinear solver. Prefer dual=False when n_samples > n_features."
      },
      "fit_intercept": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": true,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Specifies if a constant (a.k.a. bias or intercept) should be added to the decision function."
      },
      "intercept_scaling": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": 1,
        "structural_type": "float",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Useful only when the solver 'liblinear' is used and self.fit_intercept is set to True. In this case, x becomes [x, self.intercept_scaling], i.e. a \"synthetic\" feature with constant value equal to intercept_scaling is appended to the instance vector. The intercept becomes ``intercept_scaling * synthetic_feature_weight``.  Note! the synthetic feature weight is subject to l1/l2 regularization as all other features. To lessen the effect of regularization on synthetic feature weight (and therefore on the intercept) intercept_scaling has to be increased."
      },
      "class_weight": {
        "type": "d3m.metadata.hyperparams.Union",
        "default": null,
        "structural_type": "typing.Union[NoneType, str]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Weights associated with classes in the form ``{class_label: weight}``. If not given, all classes are supposed to have weight one.  The \"balanced\" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as ``n_samples / (n_classes * np.bincount(y))``.  Note that these weights will be multiplied with sample_weight (passed through the fit method) if sample_weight is specified.  .. versionadded:: 0.17 *class_weight='balanced'* instead of deprecated *class_weight='auto'*.",
        "configuration": {
          "str": {
            "type": "d3m.metadata.hyperparams.Constant",
            "default": "balanced",
            "structural_type": "str",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ]
          },
          "none": {
            "type": "d3m.metadata.hyperparams.Constant",
            "default": null,
            "structural_type": "NoneType",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ]
          }
        }
      },
      "max_iter": {
        "type": "d3m.metadata.hyperparams.Bounded",
        "default": 100,
        "structural_type": "int",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Useful only for the newton-cg, sag and lbfgs solvers. Maximum number of iterations taken for the solvers to converge.",
        "lower": 0,
        "upper": null,
        "lower_inclusive": true,
        "upper_inclusive": false
      },
      "solver": {
        "type": "d3m.metadata.hyperparams.Enumeration",
        "default": "liblinear",
        "structural_type": "str",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Algorithm to use in the optimization problem.  - For small datasets, 'liblinear' is a good choice, whereas 'sag' is faster for large ones. - For multiclass problems, only 'newton-cg', 'sag' and 'lbfgs' handle multinomial loss; 'liblinear' is limited to one-versus-rest schemes. - 'newton-cg', 'lbfgs' and 'sag' only handle L2 penalty.  Note that 'sag' fast convergence is only guaranteed on features with approximately the same scale. You can preprocess the data with a scaler from sklearn.preprocessing.  .. versionadded:: 0.17 Stochastic Average Gradient descent solver.",
        "values": [
          "newton-cg",
          "lbfgs",
          "liblinear",
          "sag",
          "saga"
        ]
      },
      "tol": {
        "type": "d3m.metadata.hyperparams.Bounded",
        "default": 0.0001,
        "structural_type": "float",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Tolerance for stopping criteria.",
        "lower": 0,
        "upper": null,
        "lower_inclusive": true,
        "upper_inclusive": false
      },
      "C": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": 1.0,
        "structural_type": "float",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Inverse of regularization strength; must be a positive float. Like in support vector machines, smaller values specify stronger regularization."
      },
      "multi_class": {
        "type": "d3m.metadata.hyperparams.Enumeration",
        "default": "ovr",
        "structural_type": "str",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "Multiclass option can be either 'ovr' or 'multinomial'. If the option chosen is 'ovr', then a binary problem is fit for each label. Else the loss minimised is the multinomial loss fit across the entire probability distribution. Works only for the 'newton-cg', 'sag' and 'lbfgs' solver.  .. versionadded:: 0.18 Stochastic Average Gradient descent solver for 'multinomial' case.",
        "values": [
          "ovr",
          "multinomial"
        ]
      },
      "warm_start": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter"
        ],
        "description": "When set to True, reuse the solution of the previous call to fit as initialization, otherwise, just erase the previous solution. Useless for liblinear solver.  .. versionadded:: 0.17 *warm_start* to support *lbfgs*, *newton-cg*, *sag* solvers."
      },
      "n_jobs": {
        "type": "d3m.metadata.hyperparams.Union",
        "default": 1,
        "structural_type": "int",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter"
        ],
        "description": "Number of CPU cores used during the cross-validation loop. If given a value of -1, all cores are used.",
        "configuration": {
          "limit": {
            "type": "d3m.metadata.hyperparams.Bounded",
            "default": 1,
            "structural_type": "int",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ],
            "lower": 1,
            "upper": null,
            "lower_inclusive": true,
            "upper_inclusive": false
          },
          "all_cores": {
            "type": "d3m.metadata.hyperparams.Constant",
            "default": -1,
            "structural_type": "int",
            "semantic_types": [
              "https://metadata.datadrivendiscovery.org/types/TuningParameter"
            ]
          }
        }
      },
      "use_input_columns": {
        "type": "d3m.metadata.hyperparams.Set",
        "default": [],
        "structural_type": "typing.Sequence[int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "A set of column indices to force primitive to use as training input. If any specified column cannot be parsed, it is skipped.",
        "elements": {
          "type": "d3m.metadata.hyperparams.Hyperparameter",
          "default": -1,
          "structural_type": "int",
          "semantic_types": []
        },
        "is_configuration": false,
        "min_size": 0
      },
      "use_output_columns": {
        "type": "d3m.metadata.hyperparams.Set",
        "default": [],
        "structural_type": "typing.Sequence[int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "A set of column indices to force primitive to use as training target. If any specified column cannot be parsed, it is skipped.",
        "elements": {
          "type": "d3m.metadata.hyperparams.Hyperparameter",
          "default": -1,
          "structural_type": "int",
          "semantic_types": []
        },
        "is_configuration": false,
        "min_size": 0
      },
      "exclude_input_columns": {
        "type": "d3m.metadata.hyperparams.Set",
        "default": [],
        "structural_type": "typing.Sequence[int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "A set of column indices to not use as training inputs. Applicable only if \"use_columns\" is not provided.",
        "elements": {
          "type": "d3m.metadata.hyperparams.Hyperparameter",
          "default": -1,
          "structural_type": "int",
          "semantic_types": []
        },
        "is_configuration": false,
        "min_size": 0
      },
      "exclude_output_columns": {
        "type": "d3m.metadata.hyperparams.Set",
        "default": [],
        "structural_type": "typing.Sequence[int]",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "A set of column indices to not use as training target. Applicable only if \"use_columns\" is not provided.",
        "elements": {
          "type": "d3m.metadata.hyperparams.Hyperparameter",
          "default": -1,
          "structural_type": "int",
          "semantic_types": []
        },
        "is_configuration": false,
        "min_size": 0
      },
      "return_result": {
        "type": "d3m.metadata.hyperparams.Enumeration",
        "default": "new",
        "structural_type": "str",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Should parsed columns be appended, should they replace original columns, or should only parsed columns be returned? This hyperparam is ignored if use_semantic_types is set to false.",
        "values": [
          "append",
          "replace",
          "new"
        ]
      },
      "use_semantic_types": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Controls whether semantic_types metadata will be used for filtering columns in input dataframe. Setting this to false makes the code ignore return_result and will produce only the output dataframe"
      },
      "add_index_columns": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": false,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Also include primary index columns if input data has them. Applicable only if \"return_result\" is set to \"new\"."
      },
      "error_on_no_input": {
        "type": "d3m.metadata.hyperparams.UniformBool",
        "default": true,
        "structural_type": "bool",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter"
        ],
        "description": "Throw an exception if no input column is selected/provided. Defaults to true to behave like sklearn. To prevent pipelines from breaking set this to False."
      }
    },
    "arguments": {
      "hyperparams": {
        "type": "sklearn_wrap.SKLogisticRegression.Hyperparams",
        "kind": "RUNTIME"
      },
      "random_seed": {
        "type": "int",
        "kind": "RUNTIME",
        "default": 0
      },
      "docker_containers": {
        "type": "typing.Union[NoneType, typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]]",
        "kind": "RUNTIME",
        "default": null
      },
      "timeout": {
        "type": "typing.Union[NoneType, float]",
        "kind": "RUNTIME",
        "default": null
      },
      "iterations": {
        "type": "typing.Union[NoneType, int]",
        "kind": "RUNTIME",
        "default": null
      },
      "produce_methods": {
        "type": "typing.Sequence[str]",
        "kind": "RUNTIME"
      },
      "inputs": {
        "type": "d3m.container.pandas.DataFrame",
        "kind": "PIPELINE"
      },
      "outputs": {
        "type": "d3m.container.pandas.DataFrame",
        "kind": "PIPELINE"
      },
      "params": {
        "type": "sklearn_wrap.SKLogisticRegression.Params",
        "kind": "RUNTIME"
      }
    },
    "class_methods": {
      "can_accept": {
        "arguments": {
          "method_name": {
            "type": "str"
          },
          "arguments": {
            "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
          },
          "hyperparams": {
            "type": "sklearn_wrap.SKLogisticRegression.Hyperparams"
          }
        },
        "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]",
        "description": "Returns a metadata object describing the output of a call of ``method_name`` method under\n``hyperparams`` with primitive arguments ``arguments``, if such arguments can be accepted by the method.\nOtherwise it returns ``None`` or raises an exception.\n\nDefault implementation checks structural types of ``arguments`` expected arguments' types\nand ignores ``hyperparams``.\n\nBy (re)implementing this method, a primitive can fine-tune which arguments it accepts\nfor its methods which goes beyond just structural type checking. For example, a primitive might\noperate only on images, so it can accept numpy arrays, but only those with semantic type\ncorresponding to an image. Or it might check dimensions of an array to assure it operates\non square matrix.\n\nPrimitive arguments are a superset of method arguments. This method receives primitive arguments and\nnot just method arguments so that it is possible to implement it without a state between calls\nto ``can_accept`` for multiple methods. For example, a call to ``fit`` could during normal execution\ninfluences what a later ``produce`` call outputs. But during ``can_accept`` call we can directly have\naccess to arguments which would have been given to ``fit`` to produce metadata of the ``produce`` call.\n\nNot all primitive arguments have to be provided, only those used by ``fit``, ``set_training_data``,\nand produce methods, and those used by the ``method_name`` method itself.\n\nParameters\n----------\nmethod_name : str\n    Name of the method which would be called.\narguments : Dict[str, Union[Metadata, type]]\n    A mapping between argument names and their metadata objects (for pipeline arguments) or types (for other).\nhyperparams : Hyperparams\n    Hyper-parameters under which the method would be called during regular primitive execution.\n\nReturns\n-------\nDataMetadata\n    Metadata object of the method call result, or ``None`` if arguments are not accepted\n    by the method."
      }
    },
    "instance_methods": {
      "__init__": {
        "kind": "OTHER",
        "arguments": [
          "hyperparams",
          "random_seed",
          "docker_containers"
        ],
        "returns": "NoneType"
      },
      "fit": {
        "kind": "OTHER",
        "arguments": [
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
        "description": "Fits primitive using inputs and outputs (if any) using currently set training data.\n\nThe returned value should be a ``CallResult`` object with ``value`` set to ``None``.\n\nIf ``fit`` has already been called in the past on different training data,\nthis method fits it **again from scratch** using currently set training data.\n\nOn the other hand, caller can call ``fit`` multiple times on the same training data\nto continue fitting.\n\nIf ``fit`` fully fits using provided training data, there is no point in making further\ncalls to this method with same training data, and in fact further calls can be noops,\nor a primitive can decide to refit from scratch.\n\nIn the case fitting can continue with same training data (even if it is maybe not reasonable,\nbecause the internal metric primitive is using looks like fitting will be degrading), if ``fit``\nis called again (without setting training data), the primitive has to continue fitting.\n\nCaller can provide ``timeout`` information to guide the length of the fitting process.\nIdeally, a primitive should adapt its fitting process to try to do the best fitting possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore fitting, it should raise a ``TimeoutError`` exception to signal that fitting was\nunsuccessful in the given time. The state of the primitive after the exception should be\nas the method call has never happened and primitive should continue to operate normally.\nThe purpose of ``timeout`` is to give opportunity to a primitive to cleanly manage\nits state instead of interrupting execution from outside. Maintaining stable internal state\nshould have precedence over respecting the ``timeout`` (caller can terminate the misbehaving\nprimitive from outside anyway). If a longer ``timeout`` would produce different fitting,\nthen ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal fitting iterations (for example, epochs). For those, caller\ncan provide how many of primitive's internal iterations should a primitive do before returning.\nPrimitives should make iterations as small as reasonable. If ``iterations`` is ``None``,\nthen there is no limit on how many iterations the primitive should do and primitive should\nchoose the best amount of iterations on its own (potentially controlled through\nhyper-parameters). If ``iterations`` is a number, a primitive has to do those number of\niterations (even if not reasonable), if possible. ``timeout`` should still be respected\nand potentially less iterations can be done because of that. Primitives with internal\niterations should make ``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should fit fully, respecting only ``timeout``.\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
      },
      "fit_multi_produce": {
        "kind": "OTHER",
        "arguments": [
          "produce_methods",
          "inputs",
          "outputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.MultiCallResult",
        "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs : Outputs\n    The outputs given to ``set_training_data``.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
      },
      "get_params": {
        "kind": "OTHER",
        "arguments": [],
        "returns": "sklearn_wrap.SKLogisticRegression.Params",
        "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
      },
      "log_likelihood": {
        "kind": "OTHER",
        "arguments": [
          "outputs",
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[Outputs]",
        "description": "Returns log probability of outputs given inputs and params under this primitive:\n\nsum_i(log(p(output_i | input_i, params)))\n\nBy default it calls ``log_likelihoods`` and tries to automatically compute a sum, but subclasses can\nimplement a more efficient or even correct version.\n\nParameters\n----------\noutputs : Outputs\n    The outputs. The number of samples should match ``inputs``.\ninputs : Inputs\n    The inputs. The number of samples should match ``outputs``.\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    sum_i(log(p(output_i | input_i, params))) wrapped inside ``CallResult``.\n    The number of returned samples is always 1.\n    The number of columns should match the number of target columns in ``outputs``."
      },
      "log_likelihoods": {
        "kind": "OTHER",
        "arguments": [
          "outputs",
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[typing.Sequence[float]]",
        "description": "Returns log probability of outputs given inputs and params under this primitive:\n\nlog(p(output_i | input_i, params))\n\nParameters\n----------\noutputs : Outputs\n    The outputs. The number of samples should match ``inputs``.\ninputs : Inputs\n    The inputs. The number of samples should match ``outputs``.\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    log(p(output_i | input_i, params))) wrapped inside ``CallResult``.\n    The number of columns should match the number of target columns in ``outputs``."
      },
      "multi_produce": {
        "kind": "OTHER",
        "arguments": [
          "produce_methods",
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.MultiCallResult",
        "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
      },
      "produce": {
        "kind": "PRODUCE",
        "arguments": [
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
        "singleton": false,
        "inputs_across_samples": [],
        "description": "Produce primitive's best choice of the output for each of the inputs.\n\nThe output value should be wrapped inside ``CallResult`` object before returning.\n\nIn many cases producing an output is a quick operation in comparison with ``fit``, but not\nall cases are like that. For example, a primitive can start a potentially long optimization\nprocess to compute outputs. ``timeout`` and ``iterations`` can serve as a way for a caller\nto guide the length of this process.\n\nIdeally, a primitive should adapt its call to try to produce the best outputs possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore producing outputs, it should raise a ``TimeoutError`` exception to signal that the\ncall was unsuccessful in the given time. The state of the primitive after the exception\nshould be as the method call has never happened and primitive should continue to operate\nnormally. The purpose of ``timeout`` is to give opportunity to a primitive to cleanly\nmanage its state instead of interrupting execution from outside. Maintaining stable internal\nstate should have precedence over respecting the ``timeout`` (caller can terminate the\nmisbehaving primitive from outside anyway). If a longer ``timeout`` would produce\ndifferent outputs, then ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal iterations (for example, optimization iterations).\nFor those, caller can provide how many of primitive's internal iterations\nshould a primitive do before returning outputs. Primitives should make iterations as\nsmall as reasonable. If ``iterations`` is ``None``, then there is no limit on\nhow many iterations the primitive should do and primitive should choose the best amount\nof iterations on its own (potentially controlled through hyper-parameters).\nIf ``iterations`` is a number, a primitive has to do those number of iterations,\nif possible. ``timeout`` should still be respected and potentially less iterations\ncan be done because of that. Primitives with internal iterations should make\n``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should run fully, respecting only ``timeout``.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
      },
      "set_params": {
        "kind": "OTHER",
        "arguments": [
          "params"
        ],
        "returns": "NoneType",
        "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
      },
      "set_training_data": {
        "kind": "OTHER",
        "arguments": [
          "inputs",
          "outputs"
        ],
        "returns": "NoneType",
        "description": "Sets training data of this primitive.\n\nStandard sublasses in this package do not adhere to the Liskov substitution principle when\ninheriting this method because they do not necessary accept all arguments found in the base\nclass. This means that one has to inspect which arguments are accepted at runtime, or in\nother words, one has to inspect which exactly subclass a primitive implements, if\nyou are accepting a wider range of primitives. This relaxation is allowed only for\nstandard subclasses found in this package. Primitives themselves should not break\nthe Liskov substitution principle but should inherit from a suitable base class.\n\nParameters\n----------\ninputs : Inputs\n    The inputs.\noutputs : Outputs\n    The outputs."
      }
    },
    "class_attributes": {
      "logger": "logging.Logger",
      "metadata": "d3m.metadata.base.PrimitiveMetadata"
    },
    "instance_attributes": {
      "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
      "random_seed": "int",
      "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
      "volumes": "typing.Dict[str, str]",
      "temporary_directory": "typing.Union[NoneType, str]"
    },
    "params": {
      "coef_": "typing.Union[NoneType, numpy.ndarray]",
      "intercept_": "typing.Union[NoneType, numpy.ndarray]",
      "n_iter_": "typing.Union[NoneType, numpy.ndarray]",
      "classes_": "typing.Union[NoneType, numpy.ndarray]",
      "input_column_names": "typing.Union[NoneType, typing.Any]",
      "target_names_": "typing.Union[NoneType, typing.Sequence[typing.Any]]",
      "training_indices_": "typing.Union[NoneType, typing.Sequence[int]]",
      "target_column_indices_": "typing.Union[NoneType, typing.Sequence[int]]",
      "target_columns_metadata_": "typing.Union[NoneType, typing.List[collections.OrderedDict]]"
    }
  },
  "structural_type": "sklearn_wrap.SKLogisticRegression.SKLogisticRegression",
  "description": "Primitive wrapping for sklearn LogisticRegression\n`sklearn documentation <https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html>`_\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
  "digest": "cdcc79c6cee40349bd0151f5d9a5d3b5913a70f66b18b04053c24c4fc40d449e"
}