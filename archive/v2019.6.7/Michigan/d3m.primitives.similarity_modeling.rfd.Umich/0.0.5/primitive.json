{
    "id": "273b03a2-5c9e-4a2c-93f2-18d293f2994d",
    "version": "0.0.5",
    "name": "RFD",
    "description": "Primitive for learning Random Forest Distance metrics and returning a pairwise\ndistance matrix between two sets of data.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "keywords": [
        "distance",
        "metric learning",
        "weakly supervised",
        "decision tree",
        "random forest"
    ],
    "source": {
        "name": "Michigan",
        "contact": "mailto:davjoh@umich.edu",
        "uris": [
            "https://github.com/dvdmjohnson/d3m_michigan_primitives/blob/master/spider/distance/rfd/rfd.py",
            "https://github.com/dvdmjohnson/d3m_michigan_primitives"
        ],
        "citation": "@inproceedings{xiong2012random, \n                title={Random forests for metric learning with implicit pairwise position dependence},\n                author={Xiong, Caiming and Johnson, David and Xu, Ran and Corso, Jason J},\n                booktitle={Proceedings of the 18th ACM SIGKDD international conference on Knowledge discovery and data mining},\n                pages={958--966},\n                year={2012},\n                organization={ACM}"
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/dvdmjohnson/d3m_michigan_primitives.git@aa14dd2ad88b7d580779f42eec8a7d0a9632f4c4#egg=spider"
        },
        {
            "type": "UBUNTU",
            "package": "ffmpeg",
            "version": "7:2.8.11-0ubuntu0.16.04.1"
        }
    ],
    "python_path": "d3m.primitives.similarity_modeling.rfd.Umich",
    "hyperparams_to_tune": [
        "class_cons",
        "num_trees"
    ],
    "algorithm_types": [
        "RANDOM_FOREST",
        "ENSEMBLE_LEARNING"
    ],
    "primitive_family": "SIMILARITY_MODELING",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "spider.distance.rfd.rfd.RFD",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.numpy.ndarray",
            "InputLabels": "d3m.container.numpy.ndarray",
            "Outputs": "d3m.container.numpy.ndarray",
            "Params": "spider.distance.rfd.rfd.RFDParams",
            "Hyperparams": "spider.distance.rfd.rfd.RFDHyperparams"
        },
        "interfaces_version": "2019.6.7",
        "interfaces": [
            "distance.PairwiseDistanceLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "class_cons": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 1000,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "the number of pairwise constraints per class to sample from the training labels",
                "lower": 10,
                "upper": null,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "num_trees": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 500,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter",
                    "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter"
                ],
                "description": "the number of trees the metric forest should contain",
                "lower": 10,
                "upper": 2000,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "min_node_size": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 1,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "the stopping criterion for tree splitting",
                "lower": 1,
                "upper": 50,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "n_jobs": {
                "type": "d3m.metadata.hyperparams.Union",
                "default": -1,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter"
                ],
                "description": "the number of separate processes to run; if -1, will be set equal to number of cores.",
                "configuration": {
                    "enum": {
                        "type": "d3m.metadata.hyperparams.Enumeration",
                        "default": -1,
                        "structural_type": "int",
                        "semantic_types": [],
                        "values": [
                            -1
                        ]
                    },
                    "bounded": {
                        "type": "d3m.metadata.hyperparams.Bounded",
                        "default": 1,
                        "structural_type": "int",
                        "semantic_types": [],
                        "lower": 1,
                        "upper": 128,
                        "lower_inclusive": true,
                        "upper_inclusive": true
                    }
                }
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "spider.distance.rfd.rfd.RFDHyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "docker_containers": {
                "type": "typing.Union[NoneType, typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]]",
                "kind": "RUNTIME",
                "default": null
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.numpy.ndarray",
                "kind": "PIPELINE"
            },
            "second_inputs": {
                "type": "d3m.container.numpy.ndarray",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.numpy.ndarray",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "spider.distance.rfd.rfd.RFDParams",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {
            "can_accept": {
                "arguments": {
                    "method_name": {
                        "type": "str"
                    },
                    "arguments": {
                        "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
                    },
                    "hyperparams": {
                        "type": "spider.distance.rfd.rfd.RFDHyperparams"
                    }
                },
                "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]",
                "description": "Returns a metadata object describing the output of a call of ``method_name`` method under\n``hyperparams`` with primitive arguments ``arguments``, if such arguments can be accepted by the method.\nOtherwise it returns ``None`` or raises an exception.\n\nDefault implementation checks structural types of ``arguments`` expected arguments' types\nand ignores ``hyperparams``.\n\nBy (re)implementing this method, a primitive can fine-tune which arguments it accepts\nfor its methods which goes beyond just structural type checking. For example, a primitive might\noperate only on images, so it can accept numpy arrays, but only those with semantic type\ncorresponding to an image. Or it might check dimensions of an array to assure it operates\non square matrix.\n\nPrimitive arguments are a superset of method arguments. This method receives primitive arguments and\nnot just method arguments so that it is possible to implement it without a state between calls\nto ``can_accept`` for multiple methods. For example, a call to ``fit`` could during normal execution\ninfluences what a later ``produce`` call outputs. But during ``can_accept`` call we can directly have\naccess to arguments which would have been given to ``fit`` to produce metadata of the ``produce`` call.\n\nNot all primitive arguments have to be provided, only those used by ``fit``, ``set_training_data``,\nand produce methods, and those used by the ``method_name`` method itself.\n\nParameters\n----------\nmethod_name : str\n    Name of the method which would be called.\narguments : Dict[str, Union[Metadata, type]]\n    A mapping between argument names and their metadata objects (for pipeline arguments) or types (for other).\nhyperparams : Hyperparams\n    Hyper-parameters under which the method would be called during regular primitive execution.\n\nReturns\n-------\nDataMetadata\n    Metadata object of the method call result, or ``None`` if arguments are not accepted\n    by the method."
            }
        },
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed",
                    "docker_containers"
                ],
                "returns": "NoneType",
                "description": "Primitive for learning Random Forest Distance metrics and returning a pairwise\ndistance matrix between two sets of data."
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Fit the random forest distance to a set of labeled data by sampling and fitting\nto pairwise constraints.\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "second_inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The first set of collections of instances.\ninput_labels : InputLabels\n    A set of class labels for the inputs.\nsecond_inputs : Inputs\n    The second set of collections of instances.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "spider.distance.rfd.rfd.RFDParams",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "second_inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The first set of collections of instances.\nsecond_inputs : Inputs\n    The second set of collections of instances.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "second_inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.numpy.ndarray]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Compute the distance matrix between vector arrays inputs and\nsecond_inputs, yielding an output of shape n by m (where n and m are\nthe number of instances in inputs and second_inputs respectively).\n\nBoth inputs must match the dimensionality of the training data.\nThe same array may be input twice in order to generate an\n(inverted) kernel matrix for use in clustering, etc.\n\nParameters\n----------\ninputs : Inputs\n    The first set of collections of instances.\nsecond_inputs : Inputs\n    The second set of collections of instances.\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    A n by m distance matrix describing the relationship between each instance in inputs[0] and each instance\n    in inputs[1] (n and m are the number of instances in inputs[0] and inputs[1], respectively),\n    wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets training data of this primitive.\n\nParameters\n----------\ninputs : Inputs\n    The inputs.\ninput_labels : InputLabels\n    A set of class labels for the inputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "fitted": "bool",
            "d": "int",
            "rf": "sklearn.ensemble.forest.ExtraTreesRegressor"
        }
    },
    "structural_type": "spider.distance.rfd.rfd.RFD",
    "digest": "644d5b3cfbc6fd57e5b45085765313e39e9c8dee3ea1c3b20f5afad5983c0107"
}
